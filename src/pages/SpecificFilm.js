import React from 'react';
import { MDBContainer,MDBRow, MDBCol,MDBTooltip, MDBBtn,MDBIcon} from 'mdbreact'
import { viewSpecificFilm, getCharacter,getVehicle,getStarship } from '../reducer/reducer';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'

class SpecificFilm extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      filmDetails : [],
      listCharacter : [],
      listVehicles : [],
      listStarship : [],
      showVehicles : [],
      showListCharacter: [],
      showStarhips : [],
    }
  }

  componentDidMount(){
    this.handleGetSpecificFilm()
  }

  //this function is used to get the specific film from the api
  handleGetSpecificFilm = async () =>{
    await this.props.viewSpecificFilm(this.props.match.params.id)
    let detail = this.props.viewSpecificFilmState
    this.setState({filmDetails:detail, listCharacter:detail.characters, listVehicles: detail.vehicles, listStarship: detail.starships})
    this.handleGetCharacters()
    this.handleGetVehicles()
    this.handleGetStarship()
  }

  //this function is used to get the list of characters
  handleGetCharacters = async () =>{
    let {listCharacter} = this.state
    let characters = []
    listCharacter.map(async(value)=>{
       await this.props.getCharacter(value.split('/')[5])
       characters.push(this.props.getCharacterState)
       this.setState({showListCharacter:characters})
    })
  }

  //this function is used to get the list of vehicles
  handleGetVehicles = async () =>{
    let {listVehicles} = this.state
    let vehicle = []
    listVehicles.map(async(value)=>{
       await this.props.getVehicle(value.split('/')[5])
       vehicle.push(this.props.getVehicleState)
       this.setState({showVehicles:vehicle})
    })
  }

   //this function is used to get the list of star ships
  handleGetStarship = async () =>{
    let {listStarship} = this.state
    let starships = []
    listStarship.map(async(value)=>{
       await this.props.getStarship(value.split('/')[5])
       starships.push(this.props.getStarShipState)
       this.setState({showStarhips:starships})
    })
  }

  //this function is use to show the details per character using tooltip
  handleShowCharacter = () =>{
    let {showListCharacter} = this.state
    return(
      showListCharacter.map((value, index)=>{
        return(
          <MDBCol md='3' key={index}>
            <MDBTooltip placement="right" >
              <MDBBtn flat className='border-radius-30 background-color-orange text-white box-shadow-none py-1 btn-ok'>{value.name}</MDBBtn>
              <div>
                <p className='mb-0'>Name: <span className='font-color-orange'>{value.name}</span></p>
                <p className='mb-0'>Birth Year: <span className='font-color-orange'>{value.birth_year}</span></p>
                <p className='mb-0'>Gender: <span className='font-color-orange'>{value.gender}</span></p>
                <p className='mb-0'>Height: <span className='font-color-orange'>{value.height}</span></p>
                <p className='mb-0'>Weight: <span className='font-color-orange'>{value.mass}</span></p>
                <p className='mb-0'>Hair Color: <span className='font-color-orange'>{value.hair_color}</span></p>
                <p className='mb-0'>Skin Color: <span className='font-color-orange'>{value.skin_color}</span></p>
                <p className='mb-0'>Eye Color: <span className='font-color-orange'>{value.eye_color}</span></p>
              </div>
            </MDBTooltip>
          </MDBCol>
        )
      })
    )
  }  

  //this function is use to show the details per vehicle using tooltip
  handleShowVehicle = () =>{
    let {showVehicles} = this.state
    return(
      showVehicles.map((value, index)=>{
        return(
          <MDBCol md='4' key={index}>
            <MDBTooltip placement="right" >
              <MDBBtn flat className='border-radius-30 background-color-orange text-white box-shadow-none py-1 btn-ok'>{value.name}</MDBBtn>
              <div>
                <p className='mb-0'>Name: <span className='font-color-orange'>{value.name}</span></p>
                <p className='mb-0'>Model: <span className='font-color-orange'>{value.model}</span></p>
                <p className='mb-0'>Vehicle Class: <span className='font-color-orange'>{value.vehicle_class}</span></p>
                <p className='mb-0'>Manufacturer: <span className='font-color-orange'>{value.manufacturer}</span></p>
                <p className='mb-0'>Crew: <span className='font-color-orange'>{value.crew}</span></p>
                <p className='mb-0'>Cargo Capacity: <span className='font-color-orange'>{value.cargo_capacity}</span></p>
                <p className='mb-0'>Length: <span className='font-color-orange'>{value.length}</span></p>
                <p className='mb-0'>Max Atmosphering Speed: <span className='font-color-orange'>{value.max_atmosphering_speed}</span></p>
                <p className='mb-0'>Passengers: <span className='font-color-orange'>{value.passengers}</span></p>
              </div>
            </MDBTooltip>
          </MDBCol>
        )
      })
    )
  }  

  //this function is use to show the details per starship using tooltip
  handleShowStarship = () =>{
    let {showStarhips} = this.state
    return(
      showStarhips.map((value, index)=>{
        return(
          <MDBCol md='4' key={index}>
            <MDBTooltip placement="right" >
              <MDBBtn flat className='border-radius-30 background-color-orange text-white box-shadow-none py-1 btn-ok'>{value.name}</MDBBtn>
              <div>
                <p className='mb-0'>Name: <span className='font-color-orange'>{value.name}</span></p>
                <p className='mb-0'>Model: <span className='font-color-orange'>{value.model}</span></p>
                <p className='mb-0'>Vehicle Class: <span className='font-color-orange'>{value.starship_class}</span></p>
                <p className='mb-0'>MGLT: <span className='font-color-orange'>{value.MGLT}</span></p>
                <p className='mb-0'>Manufacturer: <span className='font-color-orange'>{value.manufacturer}</span></p>
                <p className='mb-0'>Crew: <span className='font-color-orange'>{value.crew}</span></p>
                <p className='mb-0'>Cargo Capacity: <span className='font-color-orange'>{value.cargo_capacity}</span></p>
                <p className='mb-0'>Length: <span className='font-color-orange'>{value.length}</span></p>
                <p className='mb-0'>Max Atmosphering Speed: <span className='font-color-orange'>{value.max_atmosphering_speed}</span></p>
                <p className='mb-0'>Passengers: <span className='font-color-orange'>{value.passengers}</span></p>
              </div>
            </MDBTooltip>
          </MDBCol>
        )
      })
    )
  }  

  //this function is used to render this film details
  renderFilmDetails = () =>{
    let {filmDetails} = this.state
    return(
      <MDBContainer className='border-radius-30 py-2 my-2'>
        <MDBCol>
          <MDBRow className='justify-content-between'>
            <p className='m-0'>Title: <span className='font-color-orange font-weight-400'>{filmDetails.title}</span></p>
            <p className='m-0'>Release Date: <span  className='font-color-orange font-weight-400'>{filmDetails.release_date}</span></p>
          </MDBRow>
        </MDBCol>
        <MDBCol>
          <MDBRow className='justify-content-between'>
          <p className='m-0'>Director: <span  className='font-color-orange font-weight-400'>{filmDetails.director}</span></p>
          </MDBRow>
        </MDBCol>
        <MDBCol>
          <MDBRow className='justify-content-between'>
            <p className='m-0'>Producer: <span  className='font-color-orange font-weight-400'>{filmDetails.producer}</span></p>
          </MDBRow>
        </MDBCol>
        <MDBCol className='text-center py-3'>
          <p className='m-0 font-color-orange font-weight-400 font-size-20'>Episode: <span>{filmDetails.episode_id}</span></p>
        </MDBCol>
        <MDBCol className='px-0 text-center'>
          <p className='m-0'><span>{filmDetails.opening_crawl}</span></p>
        </MDBCol>
        <MDBCol className='text-center py-3'>
          <span className='m-0 cursor-pointer font-color-orange font-weight-600 font-size-20' >Characters</span>
        </MDBCol>
        <MDBCol>
          <MDBRow>
            {this.handleShowCharacter()}
          </MDBRow>
        </MDBCol>
        <MDBCol className='text-center py-3'>
          <hr className='mt-0'/>
          <span className='m-0 cursor-pointer font-color-orange font-weight-600 font-size-20' >Vehicles</span>
        </MDBCol>
        <MDBCol>
          <MDBRow>
            {this.handleShowVehicle()}
          </MDBRow>
        </MDBCol>
        <MDBCol className='text-center py-3'>
          <hr className='mt-0'/>
          <span className='m-0 cursor-pointer font-color-orange font-weight-600 font-size-20' >Star Ships</span>
        </MDBCol>
        <MDBCol>
          <MDBRow>
            {this.handleShowStarship()}
          </MDBRow>
        </MDBCol>
      </MDBContainer>
    )
  }

  render() {
    let {filmDetails} = this.state
    return(
      <MDBContainer className='mb-5'>
        <MDBCol className='mt-5'>
          <MDBCol className>
          <Link to={`/`} className='btn btn-flat Ripple-parent border-radius-30 background-color-orange text-white box-shadow-none py-1 btn-ok'><span className='font-size-14 font-weight-400 cursor-pointer'><MDBIcon icon="arrow-left" /> Film List</span></Link>
            <p className='text-center font-weight-600 font-size-20 font-color-orange'>Star Wars Films</p>
            {filmDetails && this.renderFilmDetails()}
          </MDBCol>
        </MDBCol>
      </MDBContainer>
    )
  }
}

const mapStateToProps = (state) => {
	let { viewSpecificFilmState,getCharacterState,getVehicleState,getStarShipState } = state;
	return {
    viewSpecificFilmState: viewSpecificFilmState,
    getCharacterState: getCharacterState,
    getVehicleState: getVehicleState,
    getStarShipState: getStarShipState
	};
};

const mapDispatchToProps = {
  viewSpecificFilm,
  getCharacter,
  getVehicle,
  getStarship
};

export default connect(mapStateToProps, mapDispatchToProps)(SpecificFilm);

import React from 'react';
import { MDBContainer,MDBRow, MDBCol,MDBIcon,  MDBBtn, MDBModal, MDBModalBody,} from 'mdbreact'
import { getFilmList, viewSpecificFilm } from '../reducer/reducer';
import { connect } from 'react-redux';
import {Link} from 'react-router-dom'
import Input from '../components/Input'
import '../assets/styling/HomePage.css'

class HomePage extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      filmsData : [],
      search :'',
      favorite: '',
      showdata : [],
      displayFilm: [],
      modal: false,
      title: '',
      content: ''
    }
  }

  componentWillMount(){
    this.handleGetListFilms()
  }

  //this function is used to trigger the modal
  toggle = () => {
    this.setState({
      modal: !this.state.modal
    });
  }

  //this function is used for seach box event target
  handleChange = event => {
    let { filmsData } = this.state;
    let keyword = event.target.value;
    let searchItems = [];
    if(keyword.length > 0){
      filmsData.map(item => {
        if(item.title.toLowerCase().includes(keyword.toLowerCase())){
          searchItems.push(item);
        }
      })
    }else{
      searchItems = filmsData;
    }
    let fav = localStorage.getItem('favourites')
    fav = JSON.parse(fav)
    this.renderFilmsList(fav, searchItems)
  };

  //this function is used to for api call and get the list of film
  handleGetListFilms = async () =>{
    let {getFilmList} = this.props
    await getFilmList();
    this.setState({filmsData: this.props.getFilmListState.results.length>0? this.props.getFilmListState.results : []})
    let fav = localStorage.getItem('favourites')
    fav = JSON.parse(fav)
    this.renderFilmsList(fav, this.props.getFilmListState.results)
  }

  //this function is used to add and remove favorites from the local storage
  handleAddfavorite = (id, title) =>{
    let fav = localStorage.getItem('favourites')
    fav = JSON.parse(fav)
    if(fav === null){
      localStorage.setItem('favourites', JSON.stringify({ [id]:  0 }) )
      this.setState({modal: !this.state.modal, title:title, content:'has been added to my favorites.'})
    }else{
      if(fav[id === undefined]) {
        fav = {...fav, [id]: 0 }
        localStorage.setItem('favourites', JSON.stringify(fav))
        this.setState({modal: !this.state.modal, title:title, content:'has been added to my favorites.'})
      }else{
        if(fav[id] === 0 ){
          fav = {...fav, [id]: 1 }
          localStorage.setItem('favourites', JSON.stringify(fav))
          this.setState({modal: !this.state.modal, title:title, content:'has been removed from my favorites.'})
        }else{
          fav = {...fav, [id]: 0 }
          localStorage.setItem('favourites', JSON.stringify(fav))
          this.setState({modal: !this.state.modal, title:title, content:'has been added to my favorites.'})
        }
      }
    }
    
    let {filmsData} = this.state
    this.renderFilmsList(fav, filmsData)
    
  }

  //this function is used to render the list films from the homepage
  renderFilmsList = (fav, data) => {
    let temp = [];
    if(fav){
      data.map((val) => {
        if(fav[val.episode_id] === undefined){
          val = {...val, favorite: 1 }
        }else{
          val = {...val, favorite: fav[val.episode_id] }
        }
        temp.push(val)
      })
      temp.sort((a,b) => {
        return a.favorite - b.favorite;
      })
    }else{
      temp = data
    }

    let displayFilm = temp.map((value, index)=>{
        let isFav = value.favorite === 0 ? 'font-color-orange' : '';
        return(
          <MDBContainer className='container-border border-radius-30 py-2 my-2' key={index}>
            <MDBRow>
              <MDBCol>
                <Link to={`films/${value.url.split('/')[5]}`} className='color-black opacity-60  title'><span className='font-size-14 font-weight-400 cursor-pointer'>{value.title}</span></Link>
              </MDBCol>
              <MDBCol className='text-right'>
                <span className={`${isFav} favorite`} onClick={()=>{this.handleAddfavorite(value.episode_id, value.title)}}><MDBIcon far icon='heart'/></span>
              </MDBCol>
            </MDBRow>
          </MDBContainer>
        )
      })
    this.setState({ displayFilm });
  }

  //this function is used to render the modal message pop up
  renderPopUpModal = () =>{
    return(
      <MDBContainer>
        <MDBModal isOpen={this.state.modal} toggle={this.toggle} centered className='popup-modal'>
          <MDBModalBody>
            <MDBCol className='text-center'>
              <p className='m-0 font-size-20 font-color-orange font-weight-600'>{this.state.title} </p>
              <p className='opacity-80'>{this.state.content}</p>
              <MDBBtn flat className='border-radius-30 background-color-orange text-white box-shadow-none py-2 btn-ok' onClick={this.toggle}>OK</MDBBtn>
            </MDBCol>
          </MDBModalBody>
        </MDBModal>
    </MDBContainer>
    )
  }

  render() {
    let { filmsData, displayFilm } = this.state
    return(
      <MDBContainer>
        <MDBCol className='mt-5'>
          <MDBCol className>
            <p className='text-center font-weight-600 font-size-20 font-color-orange'>Star Wars Films</p>
            <Input
              onChange={this.handleChange.bind(this)}
              type='text'
              hint='Search'
              name='search'
            />
            {filmsData && displayFilm}
            {this.renderPopUpModal()}
          </MDBCol>
        </MDBCol>
      </MDBContainer>
    )
  }
}

const mapStateToProps = (state) => {
	let { getFilmListState, viewSpecificFilmState } = state;
	return {
    getFilmListState: getFilmListState,
    viewSpecificFilmState: viewSpecificFilmState
	};
};

const mapDispatchToProps = {
  getFilmList,
  viewSpecificFilm
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import HomePage from '../pages/HomePage'
import ViewFilm from '../pages/SpecificFilm'
class Routes extends React.Component {
    render() {
      return (
        <Switch>
          <Route path='/' exact component={HomePage} />
          <Route path='/films/:id' exact component={ViewFilm} />
        </Switch>
      );
    }
  }
  
  export default Routes;
  
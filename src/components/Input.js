import React from 'react';

class Input extends React.Component {
  state = {
    text: ''
  };

  //Change value of text input
  handleChange = e => {
    this.setState({ text: e.target.value });
    this.props.onChange(e);
  };

  render() {
    let { id,required, name, type, hint, text, onChange} =  this.props;
    return (
        <input
          id = {id}
          required={required}
          name={name}
          onChange={this.handleChange.bind(onChange)}
          type={type}
          className={`form-input-control form-control-sm input-active font-size-14 py-3`}
          placeholder={hint}
          value={text}
        />
    );
  }
}

export default Input;
